
class GeneticAlgorithmException (Exception):
    _code_error = None
    _msg_error = None

    def __init__(self, code: int, msg: str):
        self._code_error = code
        self._msg_error = msg

    def CodeError(self)-> int:
        return self._code_error

    def MessageError(self)-> str:
        return self._msg_error

