from Genom.Tools.ToolsException import DiapasonException

class Diapason:
    _left = None
    _right = None
    _type = None
    _value = None

    def __init__(self, left, right):
        if type(left) != type(right):
            raise DiapasonException(10, 'Not Equal Type of Boundary Diapason')
        if left > right:
            raise Diapason(12, 'Wrong Values for Borundary Diapason')
        self._type = type(left)
        self._left = left
        self._right = right

    def SetValue(self, value):
        if not type(value) == self._type:
            raise DiapasonException(15, 'You Cannot Write a Value of This Type')
        if value < self._left or value > self._right:
            raise DiapasonException(17, 'You Cannot Write a Value to Diapason')
        self._value = value

    def GetValue(self):
        if self._value is None:
            raise DiapasonException(11, 'Item Value not Set')
        return self._value