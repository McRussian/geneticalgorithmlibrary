from unittest import TestCase
from Genom.Tools.Diapason import Diapason
from Genom.Tools.ToolsException import DiapasonException
from random import randint

class TestDiapason(TestCase):
    def test_create_diapason(self):
        self.assertRaises(DiapasonException, Diapason, 1, 3.5)
        self.assertRaises(DiapasonException, Diapason, 1.3, 3)
        self.assertRaises(DiapasonException, Diapason, 11, 3.5)


    def test_set_value(self):
        d = Diapason(1, 5)
        self.assertRaises(DiapasonException, d.SetValue, 12.8)
        self.assertRaises(DiapasonException, d.SetValue, 'f')
        self.assertRaises(DiapasonException, d.SetValue, 0)
        self.assertRaises(DiapasonException, d.SetValue, 8)
        f = Diapason(-0.5, 1.3)
        self.assertRaises(DiapasonException, f.SetValue, 12.8)
        self.assertRaises(DiapasonException, f.SetValue, 'f')
        self.assertRaises(DiapasonException, f.SetValue, 0)
        self.assertRaises(DiapasonException, f.SetValue, -0.8)


    def test_get_value(self):
        count_test = 15
        for _ in range(count_test):
            left = randint(1, 20)
            right = left + randint(1, 50)
            d = Diapason(left, right)
            self.assertRaises(DiapasonException, d.GetValue)

            value = randint(left, right)
            d.SetValue(value)
            self.assertEqual(value, d.GetValue())
