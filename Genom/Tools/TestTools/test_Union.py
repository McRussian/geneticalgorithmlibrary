from unittest import TestCase
from Genom.Tools.Union import Union
from Genom.Tools.ToolsException import UnionException

class TestUnion(TestCase):
    def test_set_value(self):
        u = Union(['first', 'second', 'three'])
        self.assertRaises(UnionException, u.SetValue, 12)
        self.assertRaises(UnionException, u.SetValue, '12')
        self.assertRaises(UnionException, u.SetValue, 'fifthy')

    def test_get_value(self):
        ls = ['first', 'second', 'three']

        for item in ls:
            u = Union(ls)
            self.assertRaises(UnionException, u.GetValue)
            u.SetValue(item)
            self.assertEqual(item, u.GetValue())
