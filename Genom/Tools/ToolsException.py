import GeneticAlgorithmException

class DiapasonException(GeneticAlgorithmException.GeneticAlgorithmException):
    pass

class UnionException(GeneticAlgorithmException.GeneticAlgorithmException):
    pass