from Genom.Tools.ToolsException import UnionException

class Union:
    _value = None
    _ls_value = None

    def __init__(self, ls: list):
        self._ls_value = ls.copy()

    def SetValue(self, value):
        if not value in self._ls_value:
            raise UnionException(13, 'Bad Value for Union')
        self._value = value

    def GetValue(self):
        if self._value is None:
            raise UnionException(15, 'Item Value not Set')
        return self._value